package ballotagent

import (
	"fmt"
	"gitlab.utc.fr/lrecoupe/serveur_vote/comsoc"
	"strings"
	"time"
)

type BallotAgent struct {
	AgentId       int
	Rule          string
	DeadLine      time.Time
	VoterIds      []int
	NbAlts        int
	TieBreak      []int
	VoterProfiles comsoc.Profile
	Voted         map[int]bool
	Result        comsoc.Count
	Approvals     [][]int
}

func (ba *BallotAgent) ToString() string {
	var sb strings.Builder

	sb.WriteString(fmt.Sprintf("AgentId: %d\n", ba.AgentId))
	sb.WriteString(fmt.Sprintf("Rule: %s\n", ba.Rule))
	sb.WriteString(fmt.Sprintf("DeadLine: %s\n", ba.DeadLine))
	sb.WriteString(fmt.Sprintf("VoterIds: %v\n", ba.VoterIds))
	sb.WriteString(fmt.Sprintf("NbAlts: %d\n", ba.NbAlts))
	sb.WriteString(fmt.Sprintf("TieBreak: %v\n", ba.TieBreak))
	sb.WriteString(fmt.Sprintf("VoterProfiles: %v\n", ba.VoterProfiles))
	sb.WriteString(fmt.Sprintf("Voted: %v\n", ba.Voted))
	sb.WriteString(fmt.Sprintf("Result: %v\n", ba.Result))
	sb.WriteString(fmt.Sprintf("Approvals: %v\n", ba.Approvals))

	return sb.String()
}
