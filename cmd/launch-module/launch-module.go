package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"sync"
	"time"

	"gitlab.utc.fr/lrecoupe/serveur_vote/server"
	"gitlab.utc.fr/lrecoupe/serveur_vote/voteragent"
)

const (
	Reset  = "\033[0m"
	Red    = "\033[31m"
	Green  = "\033[32m"
	Yellow = "\033[33m"
	Blue   = "\033[34m"
	Purple = "\033[35m"
	Cyan   = "\033[36m"
	White  = "\033[37m"
)

type ballot struct {
	id   int
	alts int
	rule string
}

func doNewBallotReq(req server.NewBallotReq, url string) (res int, err error) {
	// Serialize the request to JSON
	reqData, err := json.Marshal(req)
	if err != nil {
		return 0, err
	}

	// Send the request to the server
	resp, err := http.Post(url+"/new_ballot", "application/json", bytes.NewBuffer(reqData))
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()

	// Check if the response status code is OK
	if resp.StatusCode != http.StatusCreated {
		// Read the response body to get the error message
		responseBody, err := io.ReadAll(resp.Body)
		if err != nil {
			// Handle the error that occurred while reading the response body
			fmt.Printf(Red+"Error: %s"+Reset, err)
			return -1, err
		}
		// Convert the response body to a string to get the error message
		errorMessage := string(responseBody)
		return -1, fmt.Errorf("server returned status code %d: %s", resp.StatusCode, errorMessage)
	}

	// Decode the response
	var respData struct {
		BallotId int `json:"ballot-id"`
	}
	if err := json.NewDecoder(resp.Body).Decode(&respData); err != nil {
		return 0, err
	}

	return respData.BallotId, nil
}
func main() {
	var wg sync.WaitGroup
	var lock sync.Mutex

	const ballotsAgents = 3
	ballots := make([]ballot, 0, ballotsAgents)
	// fill ballots with nil
	for i := 0; i <= ballotsAgents; i++ {
		ballots = append(ballots, ballot{-1, 0, ""})
	}
	const voterAgents = 10
	const voterAgentsPerBallot = 8
	const altsMax = 10
	const deadLineMaxSecs = 10

	const port = ":8080"
	const url = "http://localhost:8080"

	rules := []string{"majority", "borda", "approval"}

	clAgts := make([]voteragent.VoterAgent, 0, voterAgents)
	servAgt := server.NewRestServerAgent(port)

	go servAgt.Start()
	wg.Add(ballotsAgents)
	for i := 0; i < ballotsAgents; i++ {
		go func(i int) {
			defer wg.Done()

			rule := rules[rand.Intn(len(rules))]
			deadline := time.Now().Add(time.Duration(rand.Intn(deadLineMaxSecs-1)+1) * time.Second).Format(time.RFC3339)

			voterIds := make([]int, 0, 3)
			for j := 0; j < voterAgents; j++ {
				voterIds = append(voterIds, j)
			}
			rand.Shuffle(len(voterIds), func(i, j int) { voterIds[i], voterIds[j] = voterIds[j], voterIds[i] })
			voterIds = voterIds[:voterAgentsPerBallot]

			alts := rand.Intn(altsMax-2) + 2
			tieBreak := make([]int, 0, alts)
			for j := 0; j < alts; j++ {
				tieBreak = append(tieBreak, j)
			}
			rand.Shuffle(len(tieBreak), func(i, j int) { tieBreak[i], tieBreak[j] = tieBreak[j], tieBreak[i] })

			req := server.NewBallotReq{Rule: rule, Deadline: deadline, VoterIds: voterIds, NbAlts: alts, TieBreak: tieBreak}

			resp, err := doNewBallotReq(req, url)
			if err != nil {
				fmt.Println(err)
				return
			}

			lock.Lock()
			ballots[resp] = ballot{resp, alts, rule}
			lock.Unlock()
		}(i)
	}
	wg.Wait()
	for i := 0; i <= voterAgents; i++ {
		id := i + 1
		clAgts = append(clAgts, *voteragent.NewVoterAgent(id))
	}

	for i := 0; i < voterAgents; i++ {
		for j := 0; j < ballotsAgents; j++ {
			go func(i int, j int) {
				fmt.Printf(Cyan+"Voter %d is voting for ballot %d\n"+Reset, i, j)
				err := clAgts[i].Vote(j, ballots[j].rule, ballots[j].alts)
				if err != nil {
					fmt.Printf(Red+"Voter: %d, Error: %s"+Reset, i, err)
					return
				}
			}(i, j)
		}

	}

	wg.Add(ballotsAgents)
	for i := 0; i < ballotsAgents; i++ {
		go func(i int) {
			defer wg.Done()
			if ballots[i].id == -1 {
				return
			}
			fmt.Println(Purple+"Ballot", i, "is waiting for results"+Reset)
			res := -1
			err := error(nil)
			ranking := make([]int, 0, ballots[i].alts)
			for res == -1 {
				res, ranking, err = getBallotResults(i)
				if err != nil {
					fmt.Printf("Error ballot %d: %s", i, err)
				}
				if res == -2 {
					return
				}
				time.Sleep(5 * time.Second)
			}
			fmt.Printf(Green+"Ballot %d: Winner: %d, Ranking: %v\n"+Reset, i, res, ranking)
		}(i)
	}
	wg.Wait()

}

func getBallotResults(i int) (res int, ranking []int, err error) {
	// Create the request
	req := server.ResultReq{
		BallotId: i,
	}

	// Serialize the request to JSON
	reqData, err := json.Marshal(req)
	if err != nil {
		return -1, nil, err
	}

	// Send the request to the server
	url := "http://localhost:8080/result"
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(reqData))
	if err != nil {
		fmt.Sprintf("Error: %s", err)
		return -1, nil, err
	}
	defer resp.Body.Close()

	// Check if the response status code is OK
	if resp.StatusCode != http.StatusOK {
		// Read the response body to get the error message
		responseBody, err := io.ReadAll(resp.Body)
		if err != nil {
			// Handle the error that occurred while reading the response body
			return -1, nil, err
		}
		// Convert the response body to a string to get the error message
		errorMessage := string(responseBody)
		res = -1
		if resp.StatusCode == http.StatusNotFound {
			res = -2
		}
		return res, nil, fmt.Errorf("server returned status code %d: %s", resp.StatusCode, errorMessage)
	}

	// Decode the response
	var respData struct {
		Winner  int   `json:"winner"`
		Ranking []int `json:"ranking"`
	}
	if err := json.NewDecoder(resp.Body).Decode(&respData); err != nil {
		return 0, nil, err
	}

	return respData.Winner, respData.Ranking, nil

}
