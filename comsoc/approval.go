package comsoc

func ApprovalSWF(p Profile, thresholds [][]int) (Count, error) {
	count := make(Count)

	for i, prefs := range p {
		threshold := thresholds[i]
		for approval, alt := range prefs {
			count[alt] += threshold[approval]
		}
	}

	return count, nil
}

func ApprovalSCF(p Profile, thresholds [][]int) ([]Alternative, error) {
	Count, _ := ApprovalSWF(p, thresholds)
	return MaxCount(Count), nil
}
