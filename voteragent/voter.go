package voteragent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.utc.fr/lrecoupe/serveur_vote/server"
	"io"
	"math/rand"
	"net/http"
	"time"
)

const (
	Reset  = "\033[0m"
	Red    = "\033[31m"
	Green  = "\033[32m"
	Yellow = "\033[33m"
	Blue   = "\033[34m"
	Purple = "\033[35m"
	Cyan   = "\033[36m"
	White  = "\033[37m"
)

type VoterAgent struct {
	AgentId int
}

func NewVoterAgent(agentId int) *VoterAgent {
	return &VoterAgent{agentId}
}

func (v *VoterAgent) Vote(ballotid int, rule string, alts int) error {
	time.Sleep(time.Duration(rand.Intn(7)) * time.Second)
	prefs := make([]int, 0, alts)
	for i := 0; i < alts; i++ {
		prefs = append(prefs, i)
	}
	options := make([]int, 0, alts)
	for i := 0; i < alts; i++ {
		//random number between 0 and 1
		options = append(options, rand.Intn(2))
	}
	rand.Shuffle(len(prefs), func(i, j int) { prefs[i], prefs[j] = prefs[j], prefs[i] })
	rand.Shuffle(len(options), func(i, j int) { options[i], options[j] = options[j], options[i] })

	req := server.NewVoterReq{
		AgentId:  v.AgentId,
		BallotId: ballotid,
		Prefs:    prefs,
		Options:  options,
	}

	// Serialize the request to JSON
	reqData, err := json.Marshal(req)
	if err != nil {
		return err
	}

	// Send the request to the server
	url := "http://localhost:8080/vote" // Adjust the URL as needed
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(reqData))
	if err != nil {
		fmt.Sprintf(Red+"Error: %s"+Reset, err)
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		// Read the response body to get the error message
		responseBody, err := io.ReadAll(resp.Body)
		if err != nil {
			// Handle the error that occurred while reading the response body
			return err
		}
		// Convert the response body to a string to get the error message
		errorMessage := string(responseBody)
		return fmt.Errorf("server returned status code %d: %s", resp.StatusCode, errorMessage)
	}

	fmt.Printf(Green+"Voter %d successfully voted for ballot %d\n"+Reset, v.AgentId, ballotid)

	return nil

}

func createVoterReq(req server.NewVoterReq, url string) (res int, err error) {
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}

	fmt.Println("Voter Number %d: %d | %s", res, resp.StatusCode, resp.Status)

	return
}
