
### Récoupé Loïc | GI03 Alternant

# Notes personnelle:
- Ce travail à été réalisé seul. Suite à mon accident et mon opération, je n'ai pas pu assister au TD présentant le projet. Je me suis basé uniquement sur le TD3, le TD5, l'activité "Serveur de vote" et la démo "restagentdemo". N'ayant pas eu les consignes et explications en TD, j'ai pris certaines libertés sur certains points, tout en essayant au mieux de produire le résultat final attendu en utilisant les techniques de code attendue.



# Configuration

Il est conseillé de lancer plusieurs fois le programme pour observer différents scénarios. Les paramètres suivants peuvent être modifiés. La configuration par défaut est :

- 3 Ballots de vote
- 10 agents
- Deadline : Entre 1 et 10 secondes après le démarrage du serveur
- Maximum de 10 alternatives de vote
- Délai de vote : entre 0 et 7 secondes

# Les implémentations

- **Création d'agents** : Chaque agent se voit attribuer des intentions de vote (et des options d'approval, par exemple) de manière aléatoire. Ils sont identifiés par un ID unique et ont un délai avant de voter, afin d'obtenir certains votant qui arrivent "en retard" par exemple.


- **Création de ballots** : Les propriétés des ballots sont également aléatoires, incluant la règle de vote, le nombre d'alternatives et leurs deadlines. Leur rôle est de vérifier la validité des votes et de les enregistrer. Il gère aussi le calcul des résultats en utilisant la méthode de vote appropriée.


- **Le serveur** : Il est responsable de la réception des requêtes et de leur redirection vers les handlers appropriés. Il gère la création des ballots, la réception des votes et les demandes de résultats. Des mutex sont utilisés pour éviter les Data Race.


- **Logs** : Des logs ont été implémentés pour rapporter les différentes erreurs et les intentions de chaque votant. L'utilisation de couleurs permet de suivre facilement le processus de vote.

La configuration à été réalisé de sorte a ce que des requêtes soit invalides, comme l'envoi tardif de votes, des votants non enregistrés sur un ballot, ou des demandes de résultats alors que le ballot est encore ouvert.

# Points d'attentions

Durant le développement de ce projet, plusieurs défis techniques et conceptuels ont été relevés. Voici les points clés qui ont été particulièrement importants pour assurer la réussite et l'efficacité du système :

- **Gestion de la Concurrence** : Un point critique du développement qui se reflète dans le choix des structures de données et l'utilisation de mutex et/ou waitGroups.

- **Diversité des Scénarios de Vote** : L'utilisation de l'aléatoire, tout en s'assurant de la cohérence des résultats, permet d'observer différents scénarios de vote intéressants.

- **Validation des Requêtes et Gestion des Erreurs** : Toutes les erreurs possibles sont traitées et les requêtes sont minutieusement validées, avec un retour systématique vers le client.
