package server

type NewBallotReq struct {
	Rule     string `json:"rule"`
	Deadline string `json:"deadline"`
	VoterIds []int  `json:"voter-ids"`
	NbAlts   int    `json:"#alts"`
	TieBreak []int  `json:"tie-break"`
}

type NewBallotResp struct {
	BallotId int `json:"ballot-id"`
}

type NewVoterReq struct {
	AgentId  int   `json:"agent-id"`
	BallotId int   `json:"ballot-id"`
	Prefs    []int `json:"prefs"`
	Options  []int `json:"options"`
}

type ResultReq struct {
	BallotId int `json:"ballot-id"`
}

type ResultResp struct {
	Winner  int   `json:"winner"`
	Ranking []int `json:"ranking"`
}

type Response struct {
	Result int `json:"res"`
}
