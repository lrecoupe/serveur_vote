package server

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.utc.fr/lrecoupe/serveur_vote/ballotagent"
	"gitlab.utc.fr/lrecoupe/serveur_vote/comsoc"
	"log"
	"net/http"
	"sort"
	"sync"
	"time"
)

const (
	Reset  = "\033[0m"
	Red    = "\033[31m"
	Green  = "\033[32m"
	Yellow = "\033[33m"
	Blue   = "\033[34m"
	Purple = "\033[35m"
	Cyan   = "\033[36m"
	White  = "\033[37m"
)

type RestServerAgent struct {
	id          string
	addr        string
	ballots     map[int]ballotagent.BallotAgent
	voteMutex   sync.Mutex
	ballotMutex sync.Mutex
}

func NewRestServerAgent(addr string) *RestServerAgent {
	return &RestServerAgent{id: addr, addr: addr, ballots: make(map[int]ballotagent.BallotAgent)}
}

func (rsa *RestServerAgent) newBallot(w http.ResponseWriter, r *http.Request) {
	// Check if the request is a POST request
	if r.Method != http.MethodPost {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}

	// Decode the request
	var request NewBallotReq
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Validate the request
	if request.Rule != "majority" && request.Rule != "borda" && request.Rule != "approval" {
		http.Error(w, "Vote Method not implemented", http.StatusNotImplemented)
		return
	}

	// check deadline format
	deadline, err := time.Parse(time.RFC3339, request.Deadline)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// check deadline validity
	if deadline.Before(time.Now()) {
		http.Error(w, "Deadline is in the past", http.StatusBadRequest)
		return
	}

	// check number of alternatives
	if request.NbAlts < 2 {
		http.Error(w, "Number of alternatives is incorrect", http.StatusBadRequest)
		return
	}

	// check number of alternatives in tie-break
	if len(request.TieBreak) != request.NbAlts {
		http.Error(w, "Tie-break is incorrect", http.StatusBadRequest)
		return
	}

	// check tie-break validity
	seen := make(map[int]bool, request.NbAlts)
	for _, num := range request.TieBreak {
		if num < 0 || num > request.NbAlts || seen[num] {
			http.Error(w, "Tie-break is not a valid permutation", http.StatusBadRequest)
			return
		}
	}

	var approvals [][]int
	// create approval map
	if request.Rule == "approval" {
		approvals = make([][]int, 0, len(request.VoterIds))
	} else {
		approvals = make([][]int, 0, 0)
	}

	rsa.ballotMutex.Lock()
	// Create the ballot
	newBallot := ballotagent.BallotAgent{
		AgentId:       len(rsa.ballots),
		Rule:          request.Rule,
		DeadLine:      deadline,
		VoterIds:      request.VoterIds,
		NbAlts:        request.NbAlts,
		TieBreak:      request.TieBreak,
		VoterProfiles: make(comsoc.Profile, 0, len(request.VoterIds)),
		Voted:         make(map[int]bool, len(request.VoterIds)),
		Result:        make(comsoc.Count, request.NbAlts),
		Approvals:     approvals,
	}

	rsa.ballots[newBallot.AgentId] = newBallot
	rsa.ballotMutex.Unlock()

	// Send the response
	resp := NewBallotResp{BallotId: newBallot.AgentId}
	serial, err := json.Marshal(resp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusCreated)
	w.Write(serial)

	fmt.Printf(Yellow+"\nBallot Created: %s\n"+Reset, newBallot.ToString()) // Debug log

}

func (rsa *RestServerAgent) vote(w http.ResponseWriter, r *http.Request) {
	// Check if the request is a POST request
	if r.Method != http.MethodPost {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}

	// Decode the request
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	var request NewVoterReq
	err := json.Unmarshal(buf.Bytes(), &request)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// check ballotId
	ballot, ok := rsa.ballots[request.BallotId]
	if !ok {
		http.Error(w, fmt.Sprintf("Ballot %d: does not exist", request.BallotId), http.StatusNotImplemented)
		return
	}

	// check if voter is registered
	voterRegistered := false
	for _, voterId := range ballot.VoterIds {
		if voterId == request.AgentId {
			voterRegistered = true
			break
		}
	}
	if !voterRegistered {
		http.Error(w, fmt.Sprintf("Ballot %d: Voter is not registered", request.BallotId), http.StatusBadRequest)
		return
	}

	// check if ballot is still open
	if ballot.DeadLine.Before(time.Now()) {
		http.Error(w, fmt.Sprintf("Ballot %d: is closed. Late by %f seconds", request.BallotId, time.Since(ballot.DeadLine).Seconds()), http.StatusTooEarly)
		return
	}

	// check if voter already voted
	if ballot.Voted[request.AgentId] {
		http.Error(w, fmt.Sprintf("Ballot %d: Voter already voted", request.BallotId), http.StatusBadRequest)
		return
	}

	// check if prefs are valid
	if len(request.Prefs) != ballot.NbAlts {
		http.Error(w, fmt.Sprintf("Ballot %d: Number of preferences is incorrect", request.BallotId), http.StatusBadRequest)
		return
	}
	seen := make(map[int]bool, ballot.NbAlts)
	for _, num := range request.Prefs {
		if num < 0 || num > ballot.NbAlts || seen[num] {
			http.Error(w, fmt.Sprintf("Ballot %d: Preferences are not a valid permutation", request.BallotId), http.StatusBadRequest)
			return
		}
	}

	// check if options are valid
	if ballot.Rule == "approval" {
		for _, option := range request.Options {
			if option < 0 || option > ballot.NbAlts {
				http.Error(w, fmt.Sprintf("Ballot %d: Options are not valid", request.BallotId), http.StatusBadRequest)
				return
			}
		}
	}

	prefs := make([]comsoc.Alternative, len(request.Prefs))
	for i, v := range request.Prefs {
		prefs[i] = comsoc.Alternative(v)
	}
	rsa.voteMutex.Lock()
	defer rsa.voteMutex.Unlock()

	ballot, ok = rsa.ballots[request.BallotId]
	if !ok {
		http.Error(w, "Ballot does not exist", http.StatusNotFound)
		return
	}

	tempPrefs := make([]comsoc.Alternative, len(request.Prefs))
	copy(tempPrefs, prefs)

	ballot.VoterProfiles = append(ballot.VoterProfiles, tempPrefs)

	tempApprovals := make([]int, len(request.Options))
	copy(tempApprovals, request.Options)
	ballot.Approvals = append(ballot.Approvals, tempApprovals)
	ballot.Voted[request.AgentId] = true

	rsa.ballots[request.BallotId] = ballot

	w.WriteHeader(http.StatusOK)
}

func (rsa *RestServerAgent) result(w http.ResponseWriter, r *http.Request) {
	// Check if the request is a POST request
	if r.Method != http.MethodPost {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}

	// Decode the request
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	var request ResultReq
	err := json.Unmarshal(buf.Bytes(), &request)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// check ballotId
	ballot, ok := rsa.ballots[request.BallotId]
	if !ok {
		http.Error(w, "Ballot does not exist", http.StatusNotFound)
		return
	}

	// check if ballot is still open
	if ballot.DeadLine.After(time.Now()) {
		http.Error(w, "Ballot is still open", http.StatusTooEarly)
		return
	}

	if len(ballot.VoterProfiles) == 0 {
		http.Error(w, "Nobody voted", http.StatusNotFound)
		return
	}

	// check if result is already computed
	if len(ballot.Result) == 0 {
		switch ballot.Rule {
		case "majority":
			ballot.Result, _ = comsoc.MajoritySWF(ballot.VoterProfiles)
		case "borda":
			ballot.Result, _ = comsoc.BordaSWF(ballot.VoterProfiles)
		case "approval":
			ballot.Result, _ = comsoc.ApprovalSWF(ballot.VoterProfiles, ballot.Approvals)
		}
	}
	// Send the response
	resp := ResultResp{Winner: int(comsoc.MaxCount(ballot.Result)[0]), Ranking: make([]int, len(ballot.Result))}

	keys := make([]int, 0, len(ballot.Result))
	for k := range ballot.Result {
		keys = append(keys, int(k))
	}

	sort.Slice(keys, func(i, j int) bool {
		return ballot.Result[comsoc.Alternative(keys[i])] >= ballot.Result[comsoc.Alternative(keys[j])]
	})

	for i, k := range keys {
		resp.Ranking[i] = k
	}

	serial, err := json.Marshal(resp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(serial)
}

func (rsa *RestServerAgent) Start() {
	// création du multiplexer
	mux := http.NewServeMux()
	mux.HandleFunc("/new_ballot", rsa.newBallot)
	mux.HandleFunc("/vote", rsa.vote)
	mux.HandleFunc("/result", rsa.result)

	// création du serveur http
	s := &http.Server{
		Addr:           rsa.addr,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20}

	// lancement du serveur
	log.Println("Listening on", rsa.addr)
	go log.Fatal(s.ListenAndServe())
}
